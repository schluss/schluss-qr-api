"use strict";

const express = require('express');
const bodyParser = require('body-parser');
const helmet = require("helmet");
const routes = require('./routes');

const app = express();

// INIT LOGIC -----------------------------------
 
// support URL-encoded bodies
	app.use(bodyParser.urlencoded({ 
		extended: true 
	}));

// secure against different types of attacks
	app.use(helmet());

// connect routes
app.use('/', routes);

// fire up http server
/* app.listen(process.env.PORT, () => { 
	console.log('Server started on http://localhost:' + process.env.PORT);
}); */
app.listen(3000);