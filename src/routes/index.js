"use strict";

const router = require('express').Router();
const QRCode = require('easyqrcodejs-nodejs');

router.get('/', (req, res) => {
	// Generate QR code with the logo and resonse it.
	var url = 'https://schluss.org/redirect/connect/39261000-c4b2-460f-b228-24116a382526/https%3A%2F%2Fservices.schluss.app%2Fbeta_schluss%2Fconfig.json/taxdataFake';
	var logo = "../schluss-qr-code-logo.svg";

	// Options for generating the QR code with the logo.
	var options = {
		text: url,
		logo: logo, // Relative address. 
		logoWidth:80, // width. default is automatic width.
		logoHeight:80, // height. default is automatic height.
		logoBackgroundColor:'#fffff', // Logo backgroud color, Invalid when `logBgTransparent` is true.
		logoBackgroundTransparent:true, // Whether use transparent image.
	};

	// New QRCode instance with options.
	const qrcode = new QRCode(options);

	// Get standard base64 image data url text: 'data:image/png;base64, ...'
	// Response as a json.
    qrcode.toDataURL().then((data) => {
        console.log(`qrCodeImgUrl: ${data}`);
        res.json({ 'qrCodeImgUrl': data });
    });
});

// error route: when another route reports an error
router.use((err, req, res) => {
	res.status(500);
	res.send('error');	
});

module.exports = router;