
# Schluss QR API

Generates an easily embeddable Schluss QR code image to kickstart data requests

## Local development

Install needed packages via npm:   
`> npm install`

Then run:   
`> npm run servelocal`

Access the api in your browser:   
`http://localhost:8080` 

---
Under construction:


## Get access
Request an API access key here: bob@schluss.org

## Call the API

URL: /v1/[apikey]?token=[token]&configurl=[configurl]&scope=[scope]&size=[size]&type=[type]

[apikey]: must contain your API access key. Could also be sent as Authorization header instead
[token]: add your session identifier
[configurl]: urlencoded url of the url to your config.json file
[scope]: the given scope for the current datarequest
[size]: optional, default : 200. Size of the qr code image returned in pixels
[type]: option, default : gif. Image type returned

